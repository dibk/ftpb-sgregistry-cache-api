using Microsoft.Extensions.Azure;
using Microsoft.OpenApi.Models;
using Serilog;
using SgRegistry;
using SgRegistry.Services;
using SgRegistry.Services.BlobStorage;
using System.Net;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddHttpContextAccessor();
builder.Services.AddHealthChecks()
    .AddCheck<DataSourceHealthCheck>("DataSource healthcheck")
    .AddAzureBlobStorage(
    optionsFactory: of => new HealthChecks.Azure.Storage.Blobs.AzureBlobStorageHealthCheckOptions()
    { 
        ContainerName = "sgregistry",
    }
    );

builder.Services.AddAzureClients(clientBuilder =>
{
    clientBuilder.AddBlobServiceClient(builder.Configuration["AzureStorage:ConnectionString"]);//.WithName(BlobService.ClientName);   
});

builder.Services.AddMemoryCache();
builder.Services.AddScoped<IBlobService, BlobService>();
builder.Services.AddScoped<CacheService>();
builder.Services.AddHttpClient("sgregistry", c =>
{
    c.BaseAddress = new Uri(builder.Configuration["sgRegHost"]);
    c.DefaultRequestHeaders.Add("Accept", "application/vnd.sgpub.v2");
    c.DefaultRequestHeaders.Add("Accept-Encoding", "gzip,deflate,br");
}).ConfigurePrimaryHttpMessageHandler(config => new HttpClientHandler
{
    AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip | DecompressionMethods.Brotli
});


builder.Services.AddHostedService<CacheBackgroundService>();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "sgregistry", Version = "v1" });
});

builder.Services.AddAllElasticApm();

builder.Services.AddLogging(loggingBuilder =>
{
    loggingBuilder.AddSerilog();
});

builder.Host.UseSerilog();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "sgregistry v1"));
}

Logging.ConfigureLogging(builder.Configuration);
app.UseSerilogRequestLogging();
app.UseHealthChecks(new PathString("/health"));
app.UseHttpsRedirection();
app.UseRouting();
app.UseAuthorization();
app.MapControllers();

app.Run();