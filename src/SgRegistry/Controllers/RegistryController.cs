﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using SgRegistry.Data;

namespace SgRegistry.Controllers;

[Route("[controller]")]
[ApiController]
public class RegistryController : Controller
{
    private readonly IMemoryCache _memoryCache;
    private readonly ILogger<RegistryController> _logger;

    public RegistryController(IMemoryCache memoryCache, ILogger<RegistryController> logger)
    {
        _memoryCache = memoryCache;
        _logger = logger;
    }

    [HttpGet]
    public IActionResult GetForetak(string orgnr)
    {
        _logger.LogInformation("Attempting to retrieve: {@Orgnr}", orgnr);
        if (_memoryCache.TryGetValue(orgnr, out Enterprise enterprise))
            return Ok(enterprise);

        _logger.LogError("Failed to retrieve: {@Orgnr}", orgnr);
        return NotFound();
    }
}