using SgRegistry.Services.BlobStorage;
using System.Reflection;

namespace SgRegistry.Services;

public static class BlobSeed
{
    public static async Task SeedBlobStorage(IBlobService blobService)
    {
        var form = new { BlobStorageContainerId = "sgregistry", FormUrl = "sgregistry.enterprises.json" };
        var b = await blobService.GetFileAsync(form.BlobStorageContainerId, form.FormUrl);
        if (b == null)
        {
            var data = LoadFormData(form.FormUrl);
            await blobService.AddOrUpdateFileAsync(form.BlobStorageContainerId, form.FormUrl, "application/json", data);
        }
    }

    private static Stream LoadFormData(string formId)
    {
        return Assembly.GetExecutingAssembly().GetManifestResourceStream(formId);
    }
}