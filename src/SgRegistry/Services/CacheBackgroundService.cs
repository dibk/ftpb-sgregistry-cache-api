﻿using Elastic.Apm;
using Elastic.Apm.Api;
using NCrontab;
using SgRegistry.Services.BlobStorage;

namespace SgRegistry.Services;

public class CacheBackgroundService : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IConfiguration _configuration;
    private readonly IApmAgent _apmAgent;
    private readonly ILogger<CacheBackgroundService> _logger;
    private CrontabSchedule _schedule;
    private DateTime _nextRun;
    private string _cronExpression = "0 * * * *"; //Default verdi; hver time

    public CacheBackgroundService(ILogger<CacheBackgroundService> logger, IServiceProvider serviceProvider, IConfiguration configuration, IApmAgent apmAgent)
    {
        _serviceProvider = serviceProvider;
        _configuration = configuration;
        _apmAgent = apmAgent;
        _logger = logger;
    }

    private void LoadConfiguration()
    {
        _logger.LogDebug("Loading configuration");
        using (var scope = _serviceProvider.CreateScope())
        {
            var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();

            _cronExpression = configuration["ScheduleCronExpression"];
        }
    }

    private void UpdateExecutionTime()
    {
        _schedule = CrontabSchedule.Parse(_cronExpression);
        _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
        _logger.LogInformation("Next run is scheduled at {NextRun}", _nextRun.ToString());
    }

    private async Task DoWork(CancellationToken stoppingToken)
    {
        var transaction = _apmAgent.Tracer.StartTransaction($"RefreshRegistryCache", ApiConstants.TypeRequest);
        try
        {
            _logger.LogInformation("CacheBackgroundService starts refreshing data.");
            var containerName = "sgregistry";
            var filename = _configuration["sgRegFilename"];
            using (var scope = _serviceProvider.CreateScope())
            {
                var cacheService = scope.ServiceProvider.GetRequiredService<CacheService>();
                var blobService = scope.ServiceProvider.GetRequiredService<IBlobService>();

                if (!(await cacheService.RefreshDataFromAPI(stoppingToken)))
                {
                    _logger.LogWarning($"Failed to get data from: {_configuration["sgRegUrl"]}, loads from blob instead");
                    BlobItem b = await blobService.GetFileAsync(containerName, filename);
                    await cacheService.SetMemCacheAsync(b.ContentStream);
                    b.Dispose();
                }
            }
            _logger.LogInformation("CacheBackgroundService finished refreshing data.");
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occured when refreshing data.");
            transaction.CaptureException(ex);
        }
        finally
        {
            transaction.End();
        }
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("CacheBackgroundService is starting.");

        LoadConfiguration();

        _logger.LogInformation("Initial loading of data");
        await DoWork(stoppingToken);

        UpdateExecutionTime();

        do
        {
            if (DateTime.Now > _nextRun)
            {
                await DoWork(stoppingToken);

                UpdateExecutionTime();
            }
            await Task.Delay(5000, stoppingToken); //5 seconds delay
        }
        while (!stoppingToken.IsCancellationRequested);
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("CacheBackgroundService is stopping..");
        return base.StopAsync(cancellationToken);
    }
}