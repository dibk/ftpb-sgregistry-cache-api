﻿namespace SgRegistry.Services.BlobStorage;

public interface IBlobService
{
    Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, byte[] blobContent);

    Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, Stream blobContent);

    Task<BlobItem> GetFileAsync(string containerName, string fileName);

    Task DeleteFileAsync(string containerName, string filename);

    Task<bool> CreateFileIfNotExists(string containerName, string filename, string defaultContent, string mimeType);
}