﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Azure;
using System.Text;

namespace SgRegistry.Services.BlobStorage;

public class BlobService : IBlobService
{
    private readonly ILogger<BlobService> _logger;
    private readonly IAzureClientFactory<BlobServiceClient> _blobClientFactory;
    public const string ClientName = "SgBlobServiceClient";

    public BlobService(ILogger<BlobService> logger, IAzureClientFactory<BlobServiceClient> blobClientFactory)
    {
        _logger = logger;
        _blobClientFactory = blobClientFactory;
    }

    private BlobContainerClient GetBlobContainerClient(string containerName)
    {
        BlobServiceClient blobServiceClient = _blobClientFactory.CreateClient("Default");
        BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(containerName.ToLower());
        if (!containerClient.Exists())
            containerClient.CreateIfNotExists(PublicAccessType.None);

        return containerClient;
    }

    private BlobClient GetBlobClient(string containerName, string fileName)
    {
        try
        {
            BlobContainerClient containerClient = GetBlobContainerClient(containerName.ToLower());
            return containerClient.GetBlobClient(fileName.ToLower());
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, byte[] blobContent)
    {
        BlobClient blobClient = GetBlobClient(containerName.ToLower(), filename.ToLower());
        var binaryData = new BinaryData(blobContent);
        try
        {
            Response<BlobContentInfo> response = await blobClient.UploadAsync(binaryData, true, new CancellationToken());

            return blobClient.Uri.AbsoluteUri.ToString();
        }
        catch (RequestFailedException rfex)
        {
            _logger.LogError(rfex, "Unable to save {fileName} blob to container {containerName}", filename, containerName);
            throw;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Unable to save {fileName} blob to container {containerName}", filename, containerName);
            throw;
        }
    }

    public async Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, Stream blobContent)
    {
        BlobClient blobClient = GetBlobClient(containerName.ToLower(), filename.ToLower());

        var blobHttpHeader = new BlobHttpHeaders
        {
            ContentType = mimeType
        };

        try
        {
            Response<BlobContentInfo> response = await blobClient.UploadAsync(blobContent, blobHttpHeader);
            return blobClient.Uri.AbsoluteUri.ToString();
        }
        catch (RequestFailedException rfex)
        {
            _logger.LogError(rfex, "Unable to save {fileName} blob to container {containerName}", filename, containerName);
            throw;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Unable to save {fileName} blob to container {containerName}", filename, containerName);
            throw;
        }
    }

    public async Task DeleteFileAsync(string containerName, string filename)
    {
        BlobClient blobClient = GetBlobClient(containerName.ToLower(), filename.ToLower());

        await blobClient.DeleteAsync();
    }

    public async Task<BlobItem> GetFileAsync(string containerName, string fileName)
    {
        BlobClient blobClient = GetBlobClient(containerName.ToLower(), fileName.ToLower());
        try
        {
            var result = await blobClient.DownloadStreamingAsync();
            return MapToBlobItem(result, fileName);
        }
        catch (RequestFailedException rfe)
        {
            if (rfe.Status == 404)
                return null;

            throw;
        }
    }

    private static BlobItem MapToBlobItem(BlobDownloadStreamingResult blob, string fileName)
    {
        var blobItem = new BlobItem()
        {
            ContentStream = blob.Content,
            FileName = fileName,
            MimeType = blob.Details.ContentType,
            Metadata = blob.Details.Metadata,
            LastModified = blob.Details.LastModified
        };

        return blobItem;
    }

    public async Task<bool> CreateFileIfNotExists(string containerName, string filename, string defaultContent, string mimeType)
    {
        var client = GetBlobContainerClient(containerName);
        var blobClient = client.GetBlobClient(filename);
        if (!await blobClient.ExistsAsync())
        {
            var content = Encoding.UTF8.GetBytes(defaultContent);
            using (var ms = new MemoryStream(content))
                blobClient.Upload(ms, new BlobUploadOptions() { HttpHeaders = new BlobHttpHeaders() { ContentType = mimeType } });

            return true;
        }

        return false;
    }
}