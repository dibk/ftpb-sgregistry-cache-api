﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SgRegistry.Services.BlobStorage;

public class BlobItem : IDisposable
{
    private bool disposedValue;

    public string FileName { get; set; }
    public string MimeType { get; set; }
    public IDictionary<string, string> Metadata { get; set; }
    public Stream ContentStream { get; set; }
    public DateTimeOffset LastModified { get; set; }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                ContentStream.Dispose();
            }

            ContentStream = null;
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
