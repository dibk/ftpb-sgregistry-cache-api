using Elastic.Apm;
using Elastic.Apm.Api;
using Microsoft.Extensions.Caching.Memory;
using SgRegistry.Data;
using SgRegistry.Services.BlobStorage;
using System.Text.Json;

namespace SgRegistry.Services;

public class CacheService
{
    private readonly ILogger<CacheService> _logger;
    private readonly IConfiguration _configuration;
    private readonly IMemoryCache _memoryCache;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly IBlobService _blobService;
    private readonly IApmAgent _apmAgent;

    public CacheService(ILogger<CacheService> logger, IConfiguration configuration, IMemoryCache memoryCache, IHttpClientFactory httpClientFactory, IBlobService blobService, IApmAgent apmAgent)
    {
        _logger = logger;
        _configuration = configuration;
        _memoryCache = memoryCache;
        _httpClientFactory = httpClientFactory;
        _blobService = blobService;
        _apmAgent = apmAgent;
    }

    public async Task SetMemCacheAsync(Stream stream)
    {
        try
        {
            var data = await JsonSerializer.DeserializeAsync<RootObject>(stream, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });

            if (data.Enterprises != null)
            {
                _apmAgent.Tracer.CurrentTransaction?.CaptureSpan("AddingEnterprisesToMemoryCache", ApiConstants.ActionExec, () =>
                {
                    _logger.LogDebug("Adds enterprises to memory cache. Total amount {EnterprisesCount}", data.Enterprises.Count());
                    foreach (var e in data.Enterprises)
                    {
                        _memoryCache.Set(e.OrganizationalNumber, e);
                    }
                    _logger.LogDebug("Added enterprises to memory cache.");
                });
            }
        }
        catch (JsonException je)
        {
            _logger.LogError(je, "An error occured when deserializing stream.");
            throw;
        }
    }

    public async Task<bool> RefreshDataFromAPI(CancellationToken stoppingToken)
    {
        var client = _httpClientFactory.CreateClient("sgregistry");
        var requestUri = "api/enterprises.json";
        _logger.LogInformation("Attempting to get data from url: {RequestUri}", client.BaseAddress + requestUri);

        try
        {
            using var response = await client.GetAsync(requestUri, stoppingToken);
            response.EnsureSuccessStatusCode();

            _logger.LogInformation("Got data from: {RequestUri}", response.RequestMessage.RequestUri.AbsolutePath);
            var resp = await response.Content.ReadAsStreamAsync(stoppingToken);

            await SetMemCacheAsync(resp);
            resp.Seek(0, SeekOrigin.Begin);

            await _blobService.AddOrUpdateFileAsync("sgregistry", _configuration["sgRegFilename"], "application/json", resp);
            _logger.LogInformation("Updated blob {BlobName}", _configuration["sgRegFilename"]);
            return true;
        }
        catch (HttpRequestException ex)
        {
            _logger.LogError(ex, "Failed to get data from url: {RequestUri}", client.BaseAddress + requestUri);
            return false;
        }
    }
}