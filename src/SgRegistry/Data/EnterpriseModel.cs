using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace SgRegistry.Data;

public class RootObject
{
    public List<Enterprise> Enterprises { get; set; }
    public Dictionary<string, string> MetaData { get; set; }
}

public class Enterprise
{
    [Key]
    [JsonPropertyName("organizational_number")]
    public string OrganizationalNumber { get; set; }

    public string Name { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public string Www { get; set; }
    public Status Status { get; set; }
    public Address Businessaddress { get; set; }
    public Address Postaladdress { get; set; }

    [JsonPropertyName("valid_approval_areas")]
    public List<ValidApprovalArea> ValidApprovalAreas { get; set; }
}

public class Address
{
    [JsonPropertyName("line_1")]
    public string Line1 { get; set; }

    [JsonPropertyName("line_2")]
    public string Line2 { get; set; }

    [JsonPropertyName("line_3")]
    public string Line3 { get; set; }

    [JsonPropertyName("line_4")]
    public string Line4 { get; set; }

    public string Country { get; set; }

    [JsonPropertyName("postal_code")]
    public string PostalCode { get; set; }

    [JsonPropertyName("postal_town")]
    public string PostalTown { get; set; }
}

public class Status
{
    public bool Approved { get; set; }
    public string ApprovalPeriodTo { get; set; }
    public string ApprovalCertificate { get; set; }
}

public class ValidApprovalArea
{
    public string Function { get; set; }

    [JsonPropertyName("subject_area")]
    public string SubjectArea { get; set; }

    public string Pbl { get; set; }

    [JsonPropertyName("function_xml")]
    public string FunctionXml { get; set; }

    [JsonPropertyName("subject_area_xml")]
    public string SubjectAreaXml { get; set; }

    [JsonPropertyName("pbl_xml")]
    public string PblXml { get; set; }

    public string Grade { get; set; }
}