﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using SgRegistry.Data;
using SgRegistry.Services.BlobStorage;
using System.Text.Json;

namespace SgRegistry;

public class DataSourceHealthCheck : IHealthCheck
{
    private readonly IMemoryCache _memoryCache;
    private readonly IConfiguration _configuration;
    private readonly IBlobService _blobService;

    public DataSourceHealthCheck(IMemoryCache memoryCache, IConfiguration configuration, IBlobService blobService)
    {
        _memoryCache = memoryCache;
        _configuration = configuration;
        _blobService = blobService;
    }

    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        var filename = _configuration["sgRegFilename"];
        var containerName = "sgregistry";
        var blob = await _blobService.GetFileAsync(containerName, filename);

        if (blob != null)
        {
            var data = await JsonSerializer.DeserializeAsync<RootObject>(blob.ContentStream, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });

            if (data.Enterprises != null && data.Enterprises.Count > 0)
            {
                if (_memoryCache.TryGetValue(data.Enterprises.First().OrganizationalNumber, out Enterprise enterprise))
                    return HealthCheckResult.Healthy("Able to find enterprise in memoryCache");
                else
                    return HealthCheckResult.Unhealthy("Unable to find enterprise from source blob in memoryCache");
            }
        }

        return HealthCheckResult.Unhealthy($"{filename} cannot be found or is empty");
    }
}